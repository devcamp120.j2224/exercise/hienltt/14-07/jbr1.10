package com.devcamp.JBR110;

public class CircleClass {
    double radius;

    public CircleClass(){

    }

    public CircleClass(double radius){
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(){
        double sopi = 3.14;
        double dienTich = this.radius * this.radius * sopi;
        return dienTich;
    }

    public double getCircumference(){
        double sopi = 3.14;
        double chuVi = 2 * sopi * this.radius;
        return chuVi;
    }

    @Override
    public String toString(){
        return "Diện tích hình tròn: " + this.getArea() + ". Chu vi hình tròn: " + this.getCircumference();
    }
}
