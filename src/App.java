import com.devcamp.JBR110.CircleClass;

public class App {
    public static void main(String[] args) throws Exception {
        
        CircleClass circle1 = new CircleClass();
        CircleClass circle2 = new CircleClass(3.0);

        System.out.println(circle1.toString());
        System.out.println(circle2.toString());
    }
}
